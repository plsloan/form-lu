# Form-LU

This is a form that is validated based on the following criteria...

* An item is selected for completed degree
* An item is selected for subject of study
* First name is not empty
* Last name is not empty
* Email is not empty
* Telephone contains only numbers and "-" characters
* Address is not empty

## Download

This project can be downloaded by following these steps...

* Download Repo
    1. Click the "Downloads" tab
    2. Click "Download repository"

* Clone the Repo
    1. Click the "Source" tab 
    2. Click the download dropdown box for code line

## Test

1. Download repo
2. Navigate to "src" folder
3. Open index.html 
4. Test form