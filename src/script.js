console.log("Your script has loaded...");

// initialize form
degree_field = document.getElementById("dropdownDegree");
study_field = document.getElementById("dropdownStudy");
first_name = document.getElementById("first-name-field");
last_name = document.getElementById("last-name-field");
email = document.getElementById("email");
telephone = document.getElementById("telephone");
address = document.getElementById("address");
update();


// functions
function update() {
    if (degree_field.innerText == "Select") {
        study_field.disabled = true;
        study_field.innerText = "Select degree type above"
    } else if (study_field.innerText == "Select degree type above") {
        study_field.disabled = false;
        study_field.innerText = "Select"
    }
}

function validate() {
    check = false;

    if (degree_field.innerText != "Select") {
        if (study_field.innerText != "Select" && study_field.innerText != "Select degree type above") {
            if (first_name.value != "" && last_name.value != "" && email.value != "" && address.value != "") {
                if (telephone.value.replace(/-/g, "").match(/^[0-9]+$/)) {
                    console.log("Valid form...");
                    window.alert("Valid form");
                    check = true;
                }
            }
        }
    } 

    if (!check) {
        console.log("Invalid form...");
        window.alert("Invalid form");
    }

    return check;
}

// handle dropdown changes
function ascAction() {
    degree_field.innerText = "Associate's Degree";
    update();
}

function bacAction() {
    degree_field.innerText = "Bachelor's Degree";
    update();
}

function masAction() {
    degree_field.innerText = "Master's Degree";
    update();
}

function docAction() {
    degree_field.innerText = "Doctorate Degree";
    update();
}

function artAction() {
    study_field.innerText = "Art"
}

function bioAction() {
    study_field.innerText = "Biology"
}

function chmAction() {
    study_field.innerText = "Chemistry"
}

function csiAction() {
    study_field.innerText = "Computer Science"
}

function musAction() {
    study_field.innerText = "Music"
}

function theAction() {
    study_field.innerText = "Theology"
}

function zooAction() {
    study_field.innerText = "Zoology"
}